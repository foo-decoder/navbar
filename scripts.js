/**
 * Created by oleg on 19.05.15.
 */

function updateToc() {

    hideToc();

    $('.toc-container').removeClass("loading");

    $('<div id="toc" class="toc">\
        <h2>Contents</h2>\
        <ul class="nav tocify-header btn-scroll-up" style="display: none;">\
            <li><a href="#" onclick="$(\"html,body\").animate({ scrollTop: 0 }, \"slow\");$(this).blur();">\
            Menu ↑\
            </a></li>\
        </ul>\
    </div>').appendTo('.toc-container');

    $("#toc").tocify({ context: "article", selectors: "h3,h4,h5,h6",
        hashGenerator: "pretty", extendPage: false, scrollTo: 10});
}
function hideToc() {
    $("#toc").remove();
}


jQuery(document).ready(function () { updateToc(); });

// Store the original active tab
var timer, originalTab;

$('#nav-main').find('a[data-toggle]').hover(function (e) {
// On hover: preview tabs
    e.preventDefault();
    $(this).tab('show');
}).click(function (e) {
// On click: switch to summary

// If clicked 'home', reload the page
    if ($(this).attr('href') == "index.html"){
//        console.log("Home clicked","Reloading");
        window.location.reload();
        return;
    }
    e.preventDefault();

    hideToc();
    $(this).tab('show');

    $('.navbar-subnav > li.active').removeClass('active');

    var target = $($(this).attr('href'));
    $('article').html(target.find('.summary-text').html());

    originalTab = $(this);
//    console.log("Original tab set to", originalTab);
});

$('.navbar-subnav > li > a').click(function(event) {
    event.preventDefault();
//    console.log("About to load", $(this).attr('href'));
    // Set active tabs
    originalTab = $('.nav-main > li.active > a');
    $('.navbar-subnav > li.active').removeClass('active');
    $(this).parent().addClass('active');

    // Provide loading feedback…
    $('.toc-container').addClass("loading");
    $('article').addClass("loading")
        // …and start loading
        .load($(this).attr('href'), function( response, status, xhr ) {
        if ( status == "error" ) {
            hideToc();
            console.error( xhr );
            $('article').removeClass("loading").html( "<div id='loading-error' class='bg-danger'>"+
                "<h2>Loading error <strong>" + xhr.status + "</strong></h2>" +
                "<p>" + xhr.statusText + "</p></div>" );
        } else {
            $('article').removeClass("loading");
            updateToc();
        }

    });
});


$('a[data-toggle]').one('show.bs.tab', function (e) {
    if (!originalTab) {
        originalTab = e.relatedTarget; // previous active tab
//        console.log("Original tab first set to", originalTab);
    }
});

// Switch to original tab on timeout
$('.nav-horizontal').hover(function(){
    clearTimeout(timer);
}).bind('mouseleave', function(){
    clearTimeout(timer);
    timer = setTimeout(function() {
//            console.log("Reverting to", originalTab);
            $(originalTab).tab('show');
            $('.nav-horizontal li > a').blur();
        }, 500);
});

jQuery('.fixedsticky').fixedsticky();

// Scroll up button
const topOffsetToShowBtn = 227;
jQuery(window).scroll(function () {
    if (jQuery(this).scrollTop() > topOffsetToShowBtn)
        $('.btn-scroll-up').show();
    else
        $('.btn-scroll-up').hide();
});

